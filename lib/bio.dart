import 'package:flutter/material.dart';

class Bio extends StatelessWidget {
  const Bio({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("BIODATA"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 20, bottom: 10),
                  child: Center(
                    child: CircleAvatar(
                        backgroundImage: AssetImage('images/INDRA.jpg'),
                        radius: 100),
                  )
                  // Image(image: AssetImage('images/INDRA.jpg')),
                  ),
              Container(
                  child: Center(
                      child: Text('Kadek Indra Kusuma',
                          style: TextStyle(
                            fontSize: 20,
                          )))),
              Container(
                width: 300,
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.blue[600]),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Text(
                    "Kadek Indra Kusuma adalah seorang mahasiswa di lingkungan Undikhsa dan sedang menempuh pendidikan di program studi Pendidikan Teknik Informatika. Ia berasal dari Desa Joanyar"),
              )
            ],
          ),
        ));
  }
}
